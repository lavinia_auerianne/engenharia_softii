package youtube.teste;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import youtube.main.Internauta;

public class InternautaTeste {

	private static Internauta pessoa;
	
	@BeforeAll
	public static void init() {
		pessoa = new Internauta(null, 0, null, null);
		pessoa.setNome("Lavinia");
		pessoa.setIdade(23);
		pessoa.setSexo("F");
		pessoa.setLogin("lavinia@gmail.com");
	}
	
	@Test
	void testeInternautaNome() {
		assertEquals("Lavini", pessoa.getNome());
	}
	
	@Test
	void testeInternautaIdade() {
		assertEquals(23, pessoa.getIdade());
	}
	
	@Test
	void testeInternautaSexo() {
		assertEquals("F", pessoa.getSexo());
	}
	
	@Test
	void testeInternautaLogin() {
		assertEquals("lavinia@gmail.com", pessoa.getLogin());
	}
	
	/*
	@Test
	void testeComparaObjetos() {
		Internauta interTemp = pessoa; //userTemp e pessoa manipulam o mesmo objeto
		assertSame(pessoa, interTemp);
		
		System.out.println(pessoa.getNome());
		//interTemp.setNome("Joana");
		//System.out.println(pessoa.getNome());
	}
	
	@Test //antes do asserAll
	void testeInternauta() {
		assertEquals("Lavinia", pessoa.getNome()); //teste falha
		assertEquals(23, pessoa.getIdade()); //teste falha
		assertEquals("F", pessoa.getSexo());
		assertEquals("auerianne@gmail.com", pessoa.getLogin());
	}
	
	@Test
	void testeInternautaDados() {
		assertAll("Valida��o do Internauta",
		() -> assertEquals("Lavinia", pessoa.getNome()),
		() -> assertEquals(23, pessoa.getIdade()),
		() -> assertEquals("F", pessoa.getSexo()),
		() -> assertEquals("lavinia@gmail.com", pessoa.getLogin())
		);
	}*/
}
