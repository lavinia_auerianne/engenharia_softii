package youtube.main;

public class Internauta {
	private String nome;
	private int idade;
	private String sexo;
	private String login;
	private int totAssistido;
		
		public Internauta(String nome, int idade, String sexo, String login) {
			this.nome = nome;
			this.idade = idade;
			this.sexo = sexo;
			this.login = login;
			this.totAssistido = 0;	
		}
		
		
		public String getNome() {
			return nome;
		}
		
		public void setNome(String nome) {
			this.nome = nome;
		}
		
		public int getIdade() {
			return idade;
		}
		
		public void setIdade(int idade) {
			this.idade = idade;
		}
		
		public String getSexo() {
			return sexo;
		}
		
		public void setSexo(String sexo) {
			this.sexo = sexo;
		}
		
		public String getLogin() {
			return login;
		}
		
		public void setLogin(String login) {
			this.login = login;
		}
		
		public int getTotAssistido() {
			return totAssistido;
		}
		
		public void setTotAssistido(int totAssistido) {
			this.totAssistido = totAssistido;
		}
		
		@Override
		public String toString() {
			return "Internauta [" + nome + ", idade=" + idade + 
					", sexo=" + sexo +
					", login =" + login + 
					", totAssistido=" + totAssistido  + "]" + "\n";
		}

}
