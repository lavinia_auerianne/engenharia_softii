package youtube.main;

public class Visualizacao {
	private Internauta espectador;
	private Video filme;
	
	public Visualizacao(Internauta espectador, Video filme) {
		this.espectador = espectador;
		this.filme = filme;
		//this.espectador.setTotAssistido(this.espectador.getTotAssistido()+1);
		this.filme.setViews(this.filme.getViews()+1);
	}
	
	public void avaliar() {
		this.filme.setAvaliacao(5);//avaliacao recebe 5 fixo
	}
	
	public void avaliar(int nota) {
		this.filme.setAvaliacao(nota);//avaliacao recebe nota enviada
	}
	
	public void avaliar(float porc) {
		int tot = 0;
		if (porc <= 20) { //abaixo de 20% - nota 3 ...
			tot = 3;
		}else if(porc <= 50) {
			tot = 5;
		}else if (porc <= 90) {
			tot = 8;
		}else {
			tot = 10;
		}
		this.filme.setAvaliacao(tot);
	}

	public Internauta getEspectador() {
		return espectador;
	}

	public void setEspectador(Internauta espectador) {
		this.espectador = espectador;
	}

	public Video getFilme() {
		return filme;
	}

	public void setFilme(Video filme) {
		this.filme = filme;
	}

	@Override
	public String toString() {
		return "Visualizacao [espectador=" + espectador + ", filme=" + filme + "]" + "\n"+"\n";
	}
	
	
}
