package youtube.main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexao {
	
	private Connection conexaoBanco;
	private Internauta internauta;
	private ResultSet resultado = null;
	private Statement state;
	
	public Connection buscaConexao() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		
		return this.conexaoBanco = DriverManager.getConnection("jdbc:mysql://localhost:3306/esof","root", "root");
	}
	
	public void testaConexao() throws ClassNotFoundException, SQLException {
		if (buscaConexao() != null) {
			System.out.println("Conectado!");
		}else {
			System.out.println("Não Conectado!");
		}
	}
	
	public void listarInternautas() {
		try {
			String query = "SELECT * FROM PESSOA";
			this.state = this.conexaoBanco.createStatement();
			this.resultado = this.state.executeQuery(query);
			while(this.resultado.next()) {
				System.out.println("Internautas: " +this.resultado.getString("id") +", "+ this.resultado.getString("nome") +", "
						+this.resultado.getString("idade") +", "+ this.resultado.getString("sexo") +", "
						+this.resultado.getString("login") + "\n");
			}
		} catch (Exception e) {
			System.out.println("ERRO: " + e.getMessage());
		}
		
	}
	
	public Internauta inserirInternauta(String nome, int idade, String sexo, String login) {
		
		try {
			String query = "INSERT INTO PESSOA (NOME, IDADE, SEXO, LOGIN) VALUES ('" +nome +"','"+idade +"','"+sexo +"','"+login +"')";
			this.state = this.conexaoBanco.createStatement();
			this.state.executeUpdate(query);
		} catch (Exception e) {
			System.out.println("ERRO: " + e.getMessage());
		}
		return internauta;
		
	}
}
